test de la page class_user
<?php require_once('../includes/config.php');
//sert à Login et Logout le user, hash et vérifie le password 

function __construct($db){
    $this->db = $db; 
}


function is_logged_in(){
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
        return true;
    }        
}


function get_user_hash($username){

    try {

        $stmt = $this->_db->prepare(
            'SELECT password 
            FROM authors WHERE email = :email');
            $stmt->execute(array('email' => $username)
        );

        return $stmt->fetch();

    } catch(PDOException $e) {
        echo '<p class="error">'.$e->getMessage().'</p>';
    }
}
//vérifie que le password correspondent une fois haché
if($this->password_verify($password,$user['password']) == 1){

}


//si l'identifiant hashé correspond à un hash dans la base de données, alors TRUE
function login($username,$password){ 

    $hashed = $this->get_user_hash($username);
    
    if($this->password_verify($password,$hashed) == 1){
        
        $_SESSION['loggedin'] = true;
		$_SESSION['author.id'] = $user['author.id'];
		$_SESSION['author.email'] = $user['author.email'];
        return true;
    }       
}
?>