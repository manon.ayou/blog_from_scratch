<?php


try {

$stmt = $db->prepare('SELECT * FROM articles WHERE id = articles.id');
$stmt->execute(array(':id' => $_GET['id']));
$row = $stmt->fetch(); 

} catch(PDOException $e) {
echo $e->getMessage();
}
?>

<form action='' method='post'>
<input type='hidden' name='article_id' value='<?php echo $row['id'];?>'>

<p><label>Title</label><br />
<input type='text' name='article_title' value='<?php echo $row['title'];?>'></p>

<p><label>Description</label><br />
<textarea name='article_category' cols='60' rows='10'><?php echo $row['category'];?></textarea></p>

<p><label>Content</label><br />
<textarea name='article_content' cols='60' rows='10'><?php echo $row['content'];?></textarea></p>

<p><input type='submit' name='submit' value='Update'></p>

</form>


<?php 
//ajout des modifications en base de données
try {

    $stmt = $db->prepare('UPDATE blog_posts SET postTitle = :postTitle, postDesc = :postDesc, postCont = :postCont WHERE postID = :postID') ;
    $stmt->execute(array(
        ':postTitle' => $postTitle,
        ':postDesc' => $postDesc,
        ':postCont' => $postCont,
        ':postID' => $postID
    ));

    //retour à l'index
    header('Location: index.php?action=updated');
    exit;

} catch(PDOException $e) {
    echo $e->getMessage();
}