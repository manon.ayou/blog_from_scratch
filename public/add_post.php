<form action='' method='post'>

    <p><label>Title</label><br />
    <input type='text' name='nouvelArticle' value='<?php if(isset($error)){ echo $_POST['title'];}?>'></p>

    <p><label>Description</label><br />
    <textarea name='category' cols='60' rows='10'><?php if(isset($error)){ echo $_POST['category'];}?></textarea></p>

    <p><label>Content</label><br />
    <textarea name='content' cols='60' rows='10'><?php if(isset($error)){ echo $_POST['content'];}?></textarea></p>

    <p><input type='submit' name='submit' value='Submit'></p>

</form>


<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script>
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
</script>

<?php 
if(isset($_POST['submit'])){

    $_POST = array_map( 'stripslashes', $_POST );

    // récupération des informations
    extract($_POST);

    if($title ==''){
        $error[] = 'Please enter the title.';
    }

    if($category ==''){
        $error[] = 'Please enter the description.';
    }

    if($content ==''){
        $error[] = 'Please enter the content.';
    }
}
if(!isset($error)){

    try {

        // ajouter l'article à la base de données
        $stmt = $db->prepare(
        'INSERT INTO articles (title,category,content,published_at) 
        VALUES (:title, :category, :content, :published_at)') ;
        $stmt->execute(array(
            ':title' => $title,
            ':category' => $category,
            ':content' => $content,
            ':published_at' => date('Y-m-d H:i:s')
        ));

        //redirige sur l'index
        header('Location: index.php?action=added');
        exit;

    } catch(PDOException $e) {
        echo $e->getMessage();
    }

}
if(isset($error)){
    foreach($error as $error){
        echo '<p class="error">'.$error.'</p>';
    }
}