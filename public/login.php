test de la page login
<?php
//include config
require_once('../includes/config.php');
$db = new PDO('mysql:host=localhost;dbname=blogfromscratch;charset=utf8', '$dbuser', '$dbpassword');


//Si le user n'est pas connecté, redirige vers la page login
if(!$user->is_logged_in()){ header('Location: login.php'); }

?>

<form action="" method="post">
<p><label>User mail</label><input type="text" name="username" value=""  /></p>
<p><label>Password</label><input type="password" name="password" value=""  /></p>
<p><label></label><input type="submit" name="submit" value="Login"  /></p>
</form>


<?php
//renvoi à la configuration login user-class. Si TRUE, connection établie. Sinon, une erreur apparaît

if(isset($_POST['submit'])){

    $username = trim($_POST['email']);
    $password = trim($_POST['password']);
    
    if($user->login($username,$password)){ 

        //retourne sur l'index une fois connecté
        header('Location: index.php');
        exit;
    

    } else {
        $message = '<p class="error">Identifiants incorrects. Veuillez réessayer.</p>';
    }

}
if(isset($message)){ 
    echo $message; 
}

function is_logged_in(){
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
        return true;
    }        
}
 ?>